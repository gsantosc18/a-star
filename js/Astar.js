function Astar(X,Y){
  this.x = X;
  this.y = Y;
  this.nodeGrid = [];
  this.geraGride();
  this.start = null;
  this.goal = null;
  this.current = null;
  this.lastNodes = [];
  this.openNodes = [];
  this.blockedNodes = [];
  this.stado = true;
}
Astar.prototype.setStart = function(x,y){
  this.start = {x:x,y:y};
  this.current = this.start;
};
Astar.prototype.setGoal = function(x,y){
  this.goal = {x:x,y:y};
};
Astar.prototype.setBloqueio = function(x,y){
  this.blockedNodes.push({x:x,y:y});
};
Astar.prototype.setDesbloqueio = function(x,y){
  this.findNode(this.current,{status:"CLOSED"});
};
Astar.prototype.expande = function(ponto){
  /*
  * Tipo de matriz {x:x,y:y}
  */

  var X = ponto.x, Y = ponto.y;

  var pontos = [];

  if(X+1 < this.x){
    pontos.push({x:X+1,y:Y});
  }
  if(Y+1 < this.y){
    pontos.push({x:X,y:Y+1});
  }
  if(X-1 >= 0){
    pontos.push({x:X-1,y:Y});
  }
  if(Y-1 >= 0){
    pontos.push({x:X,y:Y-1});
  }

  var $this = this;

  pontos = pontos.map(function(e){
    for(var x in $this.blockedNodes){
      if($this.blockedNodes[x].x == e.x && $this.blockedNodes[x].y == e.y){
        return;
      }
    }
    for(var h in $this.lastNodes){
      if($this.lastNodes[h].x == e.x && $this.lastNodes[h].y == e.y){
        return;
      }
    }
    if($this.start.x == e.x && $this.start.y == e.y){
      return;
    }
    $this.openNodes.push(e);
    return e;
  });

  $this.openNodes.sort(function(a,b){
    return $this.calculaCurtoH(a) < $this.calculaCurtoH(b);
  });

};

// Astar.prototype.expandeNos = function(){
//   if(this.status==false){
//     return;
//   }
// 	if(this.goal.status=="FINISH"){
// 		return;
// 	}
//
//   var current = this.current;
//   var nos = this.expande(current);
//
//   if(nos.length==0){
//     this.status = false;
//     return;
//   }
//
// 	for(var x in nos){
//     this.openNodes.push(nos[x]);
// 		if(this.calculaCurtoH(nos[x])==0){
// 			this.status = false;
// 			return;
// 		}
// 	}
//
//   $this = this;
//
//   this.openNodes = this.openNodes.sort(function(a,b){
//     $this.calculaCurtoH(a) > $this.calculaCurtoH(b);
//   });
//
//   var tmparr = nos.sort(function(a,b){
// 	return 	$this.calculaCurtoH(a) > $this.calculaCurtoH(b);
//   });
//
//   var no_atual = (this.openNodes[0]);
//
//   this.findNode(this.current,{status:"NODE_OLD"});
//
//   this.lastNodes.push(no_atual);
//
//   this.openNodes.shift();
//
//   this.current = no_atual;
//
//   this.findNode(this.current,{status:"NODE_ATUAL"});
//
// };

Astar.prototype.expandeNos = function(){
  
};

Astar.prototype.findNode = function(ponto,param){
    for(var i in this.nodeGrid){
      for(var j in this.nodeGrid[i]){
        if(i==ponto.y && j==ponto.x){
          if(param!=undefined&&param.status){
            this.nodeGrid[i][j].status = param.status;
          }
          return this.nodeGrid[i][j];
        }
      }
    }
};

Astar.prototype.geraGride = function(){
  var gride_line = [];
  for(var i = 0; i<this.y; i++){
    var pontos = [];
    for(var j = 0; j<this.x; j++){
      pontos.push({x:j,y:i,status:"CLOSED"});
    }
    this.nodeGrid.push(pontos);
  }
};

Astar.prototype.getGride = function(){
  return this.nodeGrid;
};

Astar.prototype.calculaCurtoH = function(ponto){
  var abs = function(a){return (a)<0?-(a):(a);};
  var absX = abs(ponto.x-this.goal.x);
  var absY = abs(ponto.y-this.goal.y);

  return absX+absY;
};

Astar.prototype.calculaPonto = function(ponto){
    return this.subtModular(this.goal.x,ponto.x)+this.subtModular(this.goal.y,ponto.y);
};

Astar.prototype.subtModular = function(a,b){
    var subt = Math.sqrt(Math.pow((a-b),2));
    return subt;
};

Astar.prototype.gera_Manhatam = function(){
  var manhatam = [];
  for(var i = 0; i < this.y; i++){
    var arr = [];
    for(var j = 0; j < this.x; j++){
      arr.push(this.calculaCurtoH({x:j,y:i},this.goal));
    }
    manhatam.push(arr);
  }

  return manhatam;
};
