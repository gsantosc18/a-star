astar = null;
$(function(){
    var astar = null;
    $('#gerar').click(function(){
        var X = $('#xx').val();
        var Y = $('#yy').val();
        referencia = geraTabela(X,Y);
        astar = new Astar(X,Y);
    });
    $('body').delegate('td','click',function(){
        var $step = $("#step");
        var comand = $("input[name=comand]:checked");
        var col = $(this).parent().children().index($(this));
        var row = $(this).parent().parent().children().index($(this).parent());

        if(comand.val()=="partida"){
            $(".partida").removeClass("partida");
            $(this).addClass("partida");
            astar.setStart(col,row);
        }
        if(comand.val()=="chegada"){
            $(".chegada").removeClass("chegada");
            $(this).addClass("chegada");
            astar.setGoal(col,row);
            var table_manhatam = astar.gera_Manhatam();
            for(var x in table_manhatam){
              var manhatam = table_manhatam[x];
              for(var h in manhatam){
                var valor = manhatam[h];
                $("body").find("#tela table")
                    .find("tr")
                    .eq(x)
                    .find("td")
                    .eq(h)
                    .text(valor);
              }
            }
        }
        if(comand.val()=="blockeio"){
            $(this).addClass("blockeio");
            astar.setBloqueio(col,row);
        }
    });

    $("#avancar").click(function(){
        astar.expandeNos();



        for(var j in astar.openNodes){
          var node = astar.openNodes[j];
          $("body").find("#tela table")
              .find("tr")
              .eq(node.x)
              .find("td")
              .eq(node.y)
              .css("background-color","#ccc");
        }

        for(var n in astar.lastNodes){
          var node = astar.lastNodes[n];
          $("body").find("#tela table")
              .find("tr")
              .eq(node.x)
              .find("td")
              .eq(node.y)
              .css("background-color","black");
        }

        $("body").find("#tela table")
            .find("tr")
            .eq(astar.current.x)
            .find("td")
            .eq(astar.current.y)
            .css("background-color","yellow");

        // for(var i in astar.nodeGrid){
        //     for(var j in astar.nodeGrid[i]){
        //         var status = astar.nodeGrid[i][j].status;
        //         if(status=="NODE_ATUAL"){
        //             var b = $("body").find("#tela table")
        //                 .find("tr")
        //                 .eq(i)
        //                 .find("td")
        //                 .eq(j)
        //                 .css("background-color","yellow");
        //         }
				// if(status=="FINISH"){
        //             var b = $("body").find("#tela table")
        //                 .find("tr")
        //                 .eq(i)
        //                 .find("td")
        //                 .eq(j)
        //                 .css("background-color","orange");
        //         }
        //     }
        // }

    });

    $("#executar").click(function(){
      window.gerencia_interval = setInterval(function(){
        if(astar.stado==true){
          $("#avancar").click();
        }
      },100);
    });

    $("#parar").click(function(){
      if(window.gerencia_interval){
        clearInterval(window.gerencia_interval);
      }
    });
});
