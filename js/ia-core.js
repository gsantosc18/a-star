function subtModular(a,b){
    var subt = Math.sqrt(Math.pow((a-b),2));
    return subt;
}

function calculaPonto(referencia,ponto){
    return subtModular(referencia[0],ponto[0])+subtModular(referencia[1],ponto[1]);
}

referencia = [
    [1,2,3],
    [4,5,6],
    [7,8,9]
];

function expande(ponto,referencia){
    var pontos = [];

    // expende para a esquerda
    if(ponto[0]-1>0){
        pontos.push([ponto[0]-1,ponto[1]]);
    }
    // expende para a direita
    if(ponto[0]+1<referencia[0].length){
        pontos.push([ponto[0]+1,ponto[1]]);
    }
    // expande para baixo
    if(ponto[1]-1>0){
        pontos.push([ponto[0],ponto[1]-1]);
    }
    // expande para cima
    if(ponto[1]+1<referencia.length){
        pontos.push([ponto[0],ponto[1]+1]);
    }

    return pontos;
}

function geraTabela(X,Y,referencia){

    var pontos = [];

    if(referencia==0||referencia == undefined) referencia =  [5,5];

    var b = $('body');

    var table = $('table');

    table.empty();

    for(var i = 0; i < Y; i++){
        var tr = $('<tr/>');
        for(var j = 0; j < X; j++){
            var td = $('<td/>');
            td.text(" ");
            tr.append(td);
        }
        table.append(tr);
    }

    table.attr('border','1');

    b.append(table);

    return pontos;
}

function ASTAR(X,Y){
    this.x = X;
    this.y = Y;
    this.cost = 0;
    this.rota = [];
    this.openList = [];
    this.closeList = [];
    this.Manhatam = [];
    this.goal = null;
    this.start = null;
    this.current = null;
}

ASTAR.prototype.setSize = function(x,y){
  this.x = x;
  this.y = y;
}

ASTAR.prototype.setGoal = function(x,y){
  this.goal = {x:x,y:y};
  this.setCurrent(x,y);
  this.addOpenList(this.goal);
}

ASTAR.prototype.setStart = function(x,y){
  this.start = {x:x,y:y};
  this.findNode(x,y,{status:"START"});
}

ASTAR.prototype.setCurrent = function(x,y){
  this.current = {x:x,y:y};
}

ASTAR.prototype.addOpenList = function(ponto){
  this.openList.push(ponto);
};

ASTAR.prototype.removeOpenList = function(index){
  this.openList.splice(index,1);
};

ASTAR.prototype.addCloseList = function(ponto){
  this.closeList.push(ponto);
};

ASTAR.prototype.removeRota = function(index){
  this.rota.splice(index,1);
};

ASTAR.prototype.addRota = function(ponto){
  this.rota.push(ponto);
};

ASTAR.prototype.removeCloseList = function(index){
  this.closeList.splice(index,1);
};

ASTAR.prototype.getManhatam = function(){
  return this.Manhatam;
};

ASTAR.prototype.geraManhatam = function(){
  if(this.goal == null){
    console.error("O Ponto Final não foi definido");
  }
  if(this.x == null || this.y == null){
    console.error("Não foi definido um tamanho para a matriz");
  }

  for(var i = 0; i < this.y; i++){
      var pontos = [];
      var tr = $('<tr/>');
      for(var j = 0; j < this.x; j++){
          var ponto = [i,j];
          var valor = this.calculaPonto(this.goal,ponto);

          if(valor == 0){
            pontos.push({x:j,y:i,value:valor,status:'START'});
            continue;
          }

          pontos.push({x:j,y:i,value:valor,status:'CLOSE'});
      }

      this.Manhatam.push(pontos);
  }

  return this;
};

ASTAR.prototype.findNode = function(x,y,param){
    for(var i in this.Manhatam){
      for(var j in this.Manhatam[i]){
        if(i==y && j==x){
          if(param!=undefined&&param.status){
            this.Manhatam[i][j].status = param.status;
          }
          return this.Manhatam[i][j];
        }
      }
    }
};

ASTAR.prototype.subtModular = function(a,b){
    var subt = Math.sqrt(Math.pow((a-b),2));
    return subt;
};

ASTAR.prototype.calculaPonto = function(referencia,ponto){
    return this.subtModular(referencia.x,ponto[0])+this.subtModular(referencia.y,ponto[1]);
};

ASTAR.prototype.expande = function(ponto){
    var pontos = [];

    // expende para a cima
    if(ponto.x-1>=0){
        pontos.push({x:ponto.x-1,y:ponto.y});
    }
    // expende para a direita
    if(ponto.x+1<this.x){
        pontos.push({x:ponto.x+1,y:ponto.y});
    }
    // expande para cima
    if(ponto.y-1>=0){
        pontos.push({x:ponto.x,y:ponto.y-1});
    }
    // expande para baixo
    if(ponto.y+1<this.y){
        pontos.push({x:ponto.x,y:ponto.y+1});
    }
    return pontos;
};

// ASTAR.prototype.expandePontos = function(){
//   var pontos = [], tmp_openList = [];
//
//   console.log(this.openList.length);
//
//   for(var x in this.openList){
//     console.log(this.openList[x]);
//     tmp_openList.push(this.openList[x]);
//     this.addCloseList(this.openList[x]);
//     this.findNode(this.openList[x].x,this.openList[x].y,{status:"VISITED"});
//     this.removeOpenList(x);
//   }
//
//   for(var i in tmp_openList){
//     var tmp_pontos = this.expande(tmp_openList[i]);
//     var $this=this;
//     var menor_curto = 100*100;
//     var menor_ponto = null;
//
//     for(var j in tmp_pontos){
//       var ponto = this.findNode(tmp_pontos[j].x,tmp_pontos[j].y);
//
//       if(ponto.status!="CLOSE"||ponto.status=="START"){
//         if(ponto.status=="START"){
//           return this.rota;
//         }
//         continue;
//       }
//
//       if(menor_curto>this.calculateExtCost(ponto)){
//         menor_ponto = ponto;
//       }
//
//       this.findNode(tmp_pontos[j].x,tmp_pontos[j].y,{status:"OPEN"});
//       this.addOpenList(tmp_pontos[j]);
//     }
//
//     if(menor_ponto!=null){
//       this.addRota(menor_ponto);
//     }
//   }
//
// };

ASTAR.prototype.expandePontos = function(){
  var open = this.openList;
  console.log(open);
  for(var i in open){
    var exp = this.expande(open[i]);
    for(var j in exp){
      this.addOpenList(exp[j]);
    }
  }
};

ASTAR.prototype.calculateExtCost = function(current){
  var abs = function(a){return ((a)<0)?-(a):(a);};

  var absX = abs(current.x-this.start.x);
  var absY = abs(current.y-this.start.y);

  return absX+absY;
};

ASTAR.prototype.calculateHCost = function(current){

  console.log(current);

    var abs = function(a){return ((a)<0)?-(a):(a);};

    var absX = abs(current.x-this.goal.x);
    var absY = abs(current.y-this.goal.y);
    var h = 0;

    if(absX > absY) h = 14 * absY + 10 * (absX - absY);
    else h = 14 * absX + 10 * (absY - absX);

    return h;
};

ASTAR.prototype.costMoviment = function(ponto){
    return (this.current.x != ponto.x && this.current.y != ponto.y)? 14: 10;
};

ASTAR.prototype.calculateFCost = function(ponto){

};
