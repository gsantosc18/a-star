function geraTabela(X,Y,referencia){

    var table = $('table');

    table.empty();

    for(var i = 0; i < Y; i++){
        var tr = $('<tr/>');
        for(var j = 0; j < X; j++){
            var td = $('<td/>');
            td.text(" ");
            tr.append(td);
        }
        table.append(tr);
    }

    var height = $('table td').width();
    $('table td').css("height",height);
};